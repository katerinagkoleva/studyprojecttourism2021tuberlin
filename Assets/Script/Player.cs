using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public GameObject GPSWaypoint;

    public MapAnchor PlayerMapAnchor
    {
        private set;
        get;
    }
    public float GpsRelocateThreshold = 0;
    public float GpsLerpTime = 10;

    // Start is called before the first frame update
    void Start()
    {
        
        var current = this.transform.parent.rotation.eulerAngles;
        
        this.transform.parent.rotation = Quaternion.Euler(current.x, 45, current.z);

        PlayerMapAnchor = MapAnchor.Instance;
    }

    private static bool AlreadyAwoken = false;
    private void Awake()
    {
        Debug.Log("awake: " + this.transform.parent.name);
        if (!AlreadyAwoken)
        {
            DontDestroyOnLoad(this.transform.parent);
            MainGameInstance.Instance.Player = this;
        } else
        {
            // Player was already created so delete clone
            // There can only be one!
            Destroy(transform.parent.gameObject);
        }
        AlreadyAwoken = true;
    }



    LocationInfo? lastLocation;
    float lastLocationLocalTime;
    Vector3 lerpFromPosition;
    Vector3 lerpToPosition;
    bool lerp;

    float lastCompasCallibrate = 0;



    // Update is called once per frame
    void Update()
    {
        if (PlayerMapAnchor == null)
        {
            PlayerMapAnchor = MapAnchor.Instance;
        }
        if (Input.location.status == LocationServiceStatus.Running && PlayerMapAnchor != null) {
            var worldPos2D = PlayerMapAnchor.TransformFromWorldToUnity(
                new Vector2(Input.location.lastData.longitude, Input.location.lastData.latitude));
            var myPos2D = new Vector2(transform.position.x, transform.position.z);
            var distance = (myPos2D - worldPos2D).magnitude;
            var parentPos2D = new Vector2(transform.parent.position.x, transform.parent.position.z);
            var myDeltaPosition = parentPos2D - myPos2D;

            var newPos = new Vector3(worldPos2D.x + myDeltaPosition.x, transform.parent.position.y, worldPos2D.y + myDeltaPosition.y);
            if (lastLocation == null)
            {
                lastLocation = Input.location.lastData;
            }
            if (lastLocation?.timestamp < Input.location.lastData.timestamp)
            {
                lastLocation = Input.location.lastData;

                // create new WayPoint
                if (GPSWaypoint != null)
                {
                    var next = Instantiate(GPSWaypoint, this.transform.position, new Quaternion(), GameObject.Find("WayPoints").transform);
                    next.transform.position = new Vector3(worldPos2D.x, next.transform.position.y, worldPos2D.y);
                    print("w" + worldPos2D.ToString() + "np" + newPos.ToString()+"lat"+ Input.location.lastData.latitude + "long"+Input.location.lastData.longitude);
                }

                if (distance > this.GpsRelocateThreshold && !lerp)
                {
                    // if distance to real GPS is too big, then lerp to real location
                    lastLocationLocalTime = Time.time;
                    lerpToPosition = newPos;
                    lerpFromPosition = transform.parent.position;
                    lerp = true;
                }
            }


            var delta = (Time.time - lastLocationLocalTime)/GpsLerpTime;
                
            if (lerp)
            {
                transform.parent.position = Vector3.Lerp(lerpFromPosition, lerpToPosition, delta);
                if (delta > 1 )
                {
                    lerp = false;
                }
            }


        }

        float CompassTimeout = 5f;
        if (Input.compass.enabled && (Time.time - lastCompasCallibrate) > CompassTimeout)
        {
            
            


            var eu = transform.localEulerAngles;

            // only calibrate when phone is upwards
            if (Mathf.Abs(Mathf.DeltaAngle(0, eu.x)) < 45 && Mathf.Abs(Mathf.DeltaAngle(0, eu.z)) < 45)
            {
                var cameraCompassHeading = Input.compass.trueHeading + eu.z;
                var currentMeWorld = transform.eulerAngles;

                var diff = Mathf.Abs( Mathf.DeltaAngle(cameraCompassHeading, (currentMeWorld.y + 180) ));
                if (diff > 15)
                {
                    print("Compass Calibrate: original Compass:" + Input.compass.trueHeading);
                    var oldPosition = transform.position;
                    var current = this.transform.parent.rotation.eulerAngles;
                    transform.parent.rotation = Quaternion.Euler(current.x, cameraCompassHeading + 180 - eu.y, current.z);
                    transform.parent.position -= (transform.position - oldPosition);
                    lastCompasCallibrate = Time.time;

                }
            }

        
            
        }

    }


}

