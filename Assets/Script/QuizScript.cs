using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;



public class QuizScript : MonoBehaviour
{
    public PoiInformation POI;
    public Sprite right;
    public Sprite wrong;

    // Start is called before the first frame update
    void Start()
    {
        if (MainGameInstance.Instance.CurrentSelectedPoi != null)
        {
            POI = MainGameInstance.Instance.CurrentSelectedPoi;
        }
        SetTexts();

    }

    // Update is called once per frame
    void Update()
    {
        var isLast = MainGameInstance.Instance.CurrentLevel.GetNextPOI(MainGameInstance.Instance.CurrentSelectedPoi) == null;
        if (clicked && Time.time > clickedTime + 3 && isLast)
        {
            MainGameInstance.Instance.timerActive = false;
            SceneManager.LoadScene("EndofGame");
        } else if (clicked && Time.time > clickedTime + 3)
        {
            SceneManager.LoadScene("ARMainGame");
        }

    }



    private void OnValidate()
    {
#if UNITY_EDITOR
        if (POI != null)
            SetTexts();
#endif
    }

    public void SetTexts()
    {
        var answers = POI.Answers;

        // Get Answers in JSON and set text in the right TextField
        var panel = transform.Find("Panel");
        for (int i = 0; i < 4; i++)
        {
            var letters = "ABCD".ToCharArray();
            string answer = answers[i];

            var answerObj = panel.Find("answer" + (i + 1));

         //var answerButton = answerObj.GetComponent<Button>();

            Text answerText = answerObj.Find("answ" + (i + 1)).GetComponent<Text>();
            answerText.text = answer;
        }

        // Set Question Text

        string question = POI.Question;

        Text questText = panel.Find("question").Find("quest").GetComponent<Text>();
        questText.text = question;

    }
    public void ClickButton(int i, Sprite selected)
    {
        var panel = transform.Find("Panel");

        var answerObj = panel.Find("answer" + (i + 1));

        var answerImage = answerObj.GetComponent<Image>();

        answerImage.sprite = selected;
        
    }
    float clickedTime;
    bool clicked = false;
    public void SelectAnswer(int i)
    {
        if (clicked)
        {
            return;
        }
        clicked = true;
        clickedTime = Time.time;

        // Get Answers in JSON and set text in the right TextField
        var panel = transform.Find("Panel");

        var answerObj = panel.Find("answer" + (i + 1));
        
        var answerImage = answerObj.GetComponent<Image>();

        if (i == POI.CorrectAnswer)
        {
            answerImage.sprite = right;
            MainGameInstance.Instance.CoinCount += 20;
        }
        else {
            answerImage.sprite = wrong;
        }
        //MainGameInstance.Instance.CurrentSelectedPoi = MainGameInstance.Instance.CurrentLevel.GetNextPOI(MainGameInstance.Instance.CurrentSelectedPoi);
        // Deactivate All Buttons
       
    }

}
