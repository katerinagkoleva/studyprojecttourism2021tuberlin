using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// All Kind of objects for Testing and Debugging
public class DebugObject : MonoBehaviour
{
    public List<GameObject> DebugOnlyObjects = new List<GameObject>();
    public bool LoadAlternativeLevel = false;
    public MapAnchor OriginalMap;
    public MapAnchor AlternativeMap;

    public MiniMapRoute OriginalMiniMap;
    public GameObject AlternativeMiniMap;
    
    public Level AlternativeLevel;

    private void Awake()
    {
        if (!Application.isEditor)
        {
            foreach (var o in DebugOnlyObjects)
            {
                Destroy(o);
            }
        }

        if (LoadAlternativeLevel)
        {
            Destroy(OriginalMap.gameObject);
            var newMap = Instantiate(AlternativeMap);
            newMap.tag = "Origin";

            Destroy(OriginalMiniMap.transform.Find("campus").gameObject);

            var newMiniMap = Instantiate(AlternativeMiniMap);
            newMiniMap.transform.parent = OriginalMiniMap.transform;
            OriginalMiniMap.Anchor = newMiniMap.GetComponentInChildren<MapAnchor>();
            OriginalMiniMap.transform.Find("Player").GetComponent<RealWorldLocation>().MapAnchor = newMiniMap.GetComponentInChildren<MapAnchor>();

            MainGameInstance.Instance.CurrentLevel = AlternativeLevel;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
       
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
