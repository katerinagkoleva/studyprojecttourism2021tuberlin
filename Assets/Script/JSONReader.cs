using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class JSONReader : MonoBehaviour
{
    public TextAsset LevelFile;
    public int id;
    public string letter;
    public Text textField;

    // Start is called before the first frame update
    public Halak.JValue jval;
    void Start()
    {
        var root = Halak.JValue.Parse(LevelFile.text);
        var text = "";
        foreach (var poi in root["points of interest"].ToArray())
        {
            //print(poi);
            if (poi["ID"] == id)
            {
                var answers = poi["Answers"].ToArray()[0];
                //print(answers[answerLetter][0]);
                if (letter == "Q")
                {
                     text = poi["question"];
                    print(text);
                }
                else if (letter == "N")
                {
                    text = poi["POIName"];
                    print(text);
                }
                else if (letter == "S")
                {
                    text = poi["shortDescription"];
                    print(text);
                }
                else if (letter == "L")
                {
                    text = poi["description"];
                    print(text);
                }
                else { 
                    text = answers.ToObject()[letter][0].ToObject()["textAnswer"]; 
                }

               
                print(text);
                //= answers[answerLetter.ToString()][0];
                //textField = GetComponent<Text>();
                textField.text = text;
                
            }

        }
    }

}
