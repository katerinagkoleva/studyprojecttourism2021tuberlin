using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArMainGame : MonoBehaviour
{
    private static bool AlreadyAwoken = false;
    public GameObject ARSesseioOrigin;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void Awake()
    {
        if (AlreadyAwoken == false)
        {
            ARSesseioOrigin.SetActive(true);
            AlreadyAwoken = true;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
