using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ARDescription : MonoBehaviour
{
    public Text Name;
    public Text Description;
    // Start is called before the first frame update
    void Start()
    {
        Name.text = MainGameInstance.Instance.CurrentSelectedPoi.Name;
        Description.text = MainGameInstance.Instance.CurrentSelectedPoi.Description;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
