using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class imagepath : MonoBehaviour
{
    
    //public GameObject MyImagehistory;
    //public Image m_Image;
    //public Sprite m_Sprite;
    public Sprite history_img;
    public Sprite art_img;
    public GameObject MyImage;
    // Start is called before the first frame update
   // Start is called before the first frame update
    void Start()
    {
        MyImage.AddComponent(typeof(Image));
        var route = MainGameInstance.Instance.Route;
        //var image = Resources.Load<Sprite>("Canvas/GameObjectimage");
        if (route == "history")
        {
          MyImage.GetComponent<Image>().sprite = history_img;
           
        }
        if (route == "art")
        {
           MyImage.GetComponent<Image>().sprite = art_img;
        }

        
    
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
