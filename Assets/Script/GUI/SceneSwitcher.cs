using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    // Start is called before the first frame update
   //public void playGame(){
       //SceneManager.LoadScene(SceneManager.GetActivateScene().buildIndex + 1);

    public void LoadScene(){
        SceneManager.LoadScene("SceneName");
    }

    public void LoadScene1()
    {
        SceneManager.LoadScene("ChosenPath");
    }

    public void LoadScene2()
    {
        SceneManager.LoadScene("ChoosePath");
    }

     public void LoadScene3()
    {
        SceneManager.LoadScene("WarningS");
    }

    public void LoadScene4(){
        SceneManager.LoadScene("ChosenPath");
    }

    public void LoadSceneStarting()
    {
        SceneManager.LoadScene("Starting");
    }
    public void LoadSceneMainGame()
    {
        SceneManager.LoadScene("ARMainGame");
    }

    public void LoadAnyScene(string sceneName){
        SceneManager.LoadScene(sceneName);
    }

    public void LoadAnyScene2(Scene scene)
    {
        SceneManager.LoadScene(scene.name);
    }

    public void LoadSceneMCL(){
         SceneManager.LoadScene("ChoosePathClosed");
    }
    public void LoadSceneMOP(){
         SceneManager.LoadScene("ChoosePath");
    }

    public void LoadSceneCPCL(){
        SceneManager.LoadScene("2ChosenPath");
    }

    public void LoadSceneCPOP(){
        SceneManager.LoadScene("ChosenPath");
    }

    public void QuitApp()
    {
        Application.Quit();
    }


    public void LoadSceneMain(){
        SceneManager.LoadScene("(1)StartScene");
        MainGameInstance.Instance.TimeStart = 0;
        MainGameInstance.Instance.timerActive = false;
        MainGameInstance.Instance.CoinCount = 0;
        MainGameInstance.Instance.Player = null;
        MainGameInstance.Instance.CurrentSelectedPoi = null;
    }


    public void SwitchMapAndGame()
    {
        if (SceneManager.GetActiveScene().name == "ARMainGame")
        {
            SceneManager.LoadScene("3DMap");
        }

        else if (SceneManager.GetActiveScene().name == "3DMap")
        {
            SceneManager.LoadScene("ARMainGame");
        }
    }
}
