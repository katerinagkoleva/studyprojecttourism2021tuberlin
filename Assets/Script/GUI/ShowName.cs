using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ShowName : MonoBehaviour
{

    public void Awake()
    {
        var nautName = GetComponent<Text>();
        if (nautName != null)
            nautName.text = "YOU ARE PLAYING AS: "+ MainGameInstance.Instance.UserName.ToUpper();
    }
}
