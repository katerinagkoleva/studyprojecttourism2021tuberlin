using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChosenPath : MonoBehaviour
{
    public Text Title;
    public Text Info;
    public Image Icon;
    // Start is called before the first frame update
    void Start()
    {
        var route = MainGameInstance.Instance.Route;

        

        if (route == "art")
        {
            Title.text = "YOU SELECTED ART";
            Info.text =
@"YOU WILL LEARN A LOT ABOUT THE ART OF THIS INCREDIBLE PLACE

PATH DURATION: 17 MINS";
            
        } else if (route == "history")
        {
            Title.text = "YOU SELECTED HISTORY";
            Info.text =
@"YOU WILL LEARN A LOT ABOUT THE HISTORY OF THIS INCREDIBLE PLACE

PATH DURATION: 17 MINS";
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
