using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapToggler : MonoBehaviour
{
    public bool ShowMap
    {
        private set;
        get;
    }

    public GameObject MiniMapObjects;

    /// <summary>
    /// Objects that should be disabled when MiniMap is shown
    /// </summary>
    public List<GameObject> DisableObjects;

    // Start is called before the first frame update
    void Start()
    {
        ShowMap = true;
        Toggle();
    }

    // Update is called once per frame
    void Update()
    {
        // Allways keep Map in front, even when GPS is lerping
        
        MiniMapObjects.transform.position = Camera.main.transform.parent.position + (Camera.main.transform.parent.rotation* relativeDistanceCamera );
    }

    Vector3 relativeDistanceCamera;
    public void Toggle()
    {
        ShowMap = !ShowMap;
        MiniMapObjects.SetActive(ShowMap);
        foreach (var go in DisableObjects)
        {
            go.SetActive(!ShowMap);
        }

        if (ShowMap)
        {
            MiniMapObjects.transform.position = Camera.main.transform.position +
            Camera.main.transform.forward * 0.2f - new Vector3(0,0.3f,0);

           
            relativeDistanceCamera = Quaternion.Inverse(Camera.main.transform.parent.rotation) *
                (  MiniMapObjects.transform.position - Camera.main.transform.parent.position);
        }
    }

}
