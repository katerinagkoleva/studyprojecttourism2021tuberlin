using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangePhoto : MonoBehaviour
{
    // Start is called before the first frame update
    private Sprite art0;
    public GameObject img1;
    

    // Start is called before the first frame update
    void Start()
    {   
        if (img1.GetComponent<Image>()==null)
        {
            img1.AddComponent(typeof(Image));
        }
        
        if (MainGameInstance.Instance.Route == "art")
        {
            string Id = "a" + MainGameInstance.Instance.CurrentSelectedPoi.ID.ToString();
            img1.GetComponent<Image>().sprite = Resources.Load<Sprite>("Art_Short/round/" + Id);
        }
        else if (MainGameInstance.Instance.Route == "history")
        {
            string Id = "h" + MainGameInstance.Instance.CurrentSelectedPoi.ID.ToString();
            img1.GetComponent<Image>().sprite = Resources.Load<Sprite>("History_Short/round/" + Id);
        }
        
        
    }
    // Update is called once per frame
    void Update()
    {

    }


 
}
