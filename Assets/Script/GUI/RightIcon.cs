using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RightIcon : MonoBehaviour
{
    public Sprite icon_hist;
    public Sprite icon_art;
    
    // Start is called before the first frame update
    void Start()
    {
        var route = MainGameInstance.Instance.Route;
        var image = GameObject.Find("/canvas/beige panel/icon");
        if (route == "history")
        {
            var im = image.GetComponent<Image>(); 
            im.sprite = icon_hist;
        }
        if (route == "art")
        {
            var im = image.GetComponent<Image>();
            im.sprite = icon_art;
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
