using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class printtime : MonoBehaviour
{
    public Text dt;
    // Start is called before the first frame update
    
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {

        //dt = GetComponent<Text>();
        if (dt == null)
            return;
        float Time = MainGameInstance.Instance.TimeStart;
        int secondsTotal = (int)Mathf.Round(Time);
        int seconds = secondsTotal % 60;
        int minutes = secondsTotal / 60;
        dt.text = minutes.ToString("00") + ":" + seconds.ToString("00");
        
    }
}
