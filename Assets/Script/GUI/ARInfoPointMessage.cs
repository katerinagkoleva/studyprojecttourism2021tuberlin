using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


/// <summary>
/// Show the Description when SpecialCoin is clicked
/// </summary>
public class ARInfoPointMessage : MonoBehaviour
{
    public PoiInformation POI;
    public Text text;
    // Start is called before the first frame update
    void Start()
    {
        if (MainGameInstance.Instance.CurrentSelectedPoi != null)
        {
            POI = MainGameInstance.Instance.CurrentSelectedPoi;
        }
        SetText();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnValidate()
    {
        SetText();
    }

    void SetText()
    {
        if (POI != null && text != null)
            text.text = POI.Name;
    }
}
