﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChoosePathInterface : MonoBehaviour
{
    public Button NextButton;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void SelectRoute(string route)
    {
        MainGameInstance.Instance.Route = route;
        NextButton.interactable = true;
    }

    public void SelectLevel(Level level)
    {
        MainGameInstance.Instance.CurrentLevel = level;
    }
}
