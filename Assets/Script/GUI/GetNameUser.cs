using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetNameUser : MonoBehaviour
{
    public string UName;
    public GameObject inputField;
    public GameObject textDisplay;
  
    public void StoreName(){
       UName = inputField.GetComponent<Text>().text;
       textDisplay.GetComponent<Text>().text = "Welcome, " +UName;
   }
}
