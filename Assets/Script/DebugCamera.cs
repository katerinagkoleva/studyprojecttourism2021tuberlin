using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

public class DebugCamera : MonoBehaviour
{
    public GameObject Plane;

    ARPlaneManager planeManager;
    // Start is called before the first frame update
    void Start()
    {
        
        Input.gyro.enabled = true;
        //this.planeManager = transform.parent.GetComponent<ARPlaneManager>();
        //planeManager.planesChanged += PlaneManager_planesChanged;

    }

    private void PlaneManager_planesChanged(ARPlanesChangedEventArgs obj)
    {
        foreach (var o in obj.added)
        {

            Plane.transform.position = new Vector3(Plane.transform.position.x, o.center.y, Plane.transform.position.z);
        }
    }

    private static Quaternion GyroToUnity(Quaternion q)
    {
        return new Quaternion(q.x, q.y, -q.z, -q.w);
    }
    // Update is called once per frame
    void Update()
    {
        float speed = 0.1f;
        //this.transform.rotation = GyroToUnity(Input.gyro.attitude);
        if (Input.GetAxis("Horizontal") > 1)
        {
            
        }

        var direction = new Vector3(
         Input.GetAxis("Horizontal"),
            0,
            Input.GetAxis("Vertical"));

        

        this.transform.position = transform.position + transform.rotation * direction;

        //var text = GameObject.Find("Text").GetComponent<UnityEngine.UI.Text>();
        //text.text = this.transform.position.ToString();
        transform.Rotate(Vector3.up * Input.GetAxis("Debug Horizontal") * 3);
        //transform.Rotate(Vector3.up * Input.GetAxis("Mouse X") * 10);
    }

}
