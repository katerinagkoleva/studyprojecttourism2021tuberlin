using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinTouchController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
        foreach (var touch in Input.touches)
        {
            Click(touch.position);
        }

        if (Input.GetMouseButtonDown(0))
        {
            Click(Input.mousePosition);
        }
    }

    void Click(Vector2 position)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(position);
        
        if (Physics.Raycast(ray, out hit, LayerMask.GetMask("Coin")))
        {
            if (hit.collider != null)
            {
                Debug.Log("Hit object: " + hit.collider.gameObject.name);
                ICoin coin = null;

                var nc = hit.collider.gameObject.GetComponent<NormalCoin>();
                if (nc != null)
                    coin = nc;
                var sc = hit.collider.gameObject.GetComponent<SpecialCoin>();
                if (sc != null)
                    coin = sc;

                if (coin != null)
                {
                    coin.CollectCoin();
                }
            }
        }
    }
}
