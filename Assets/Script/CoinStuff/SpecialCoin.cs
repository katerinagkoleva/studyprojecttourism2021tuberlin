﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SpecialCoin : MonoBehaviour, ICoin
{
    public GameObject ArtCoin;
    public GameObject HistoryCoin;
    public PoiInformation POI;
    // Start is called before the first frame update
    void Start()
    {
        SetCoin();
    }

    // Update is called once per frame
    void Update()
    {

    }

    

    void SetCoin()
    {
        var route = MainGameInstance.Instance.Route;

        if (route == "art")
        {
            ArtCoin.SetActive(true);
            HistoryCoin.SetActive(false);
        }
        else if (route == "history")
        {
            ArtCoin.SetActive(false);
            HistoryCoin.SetActive(true);
        }
    
    }

    public void CollectCoin()
    {
        MainGameInstance.Instance.CurrentSelectedPoi = POI;
        SceneManager.LoadScene("ARInfoPoints");
    }
}
