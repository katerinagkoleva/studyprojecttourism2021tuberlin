﻿using UnityEngine;
using System.Collections;

public class NormalCoin : MonoBehaviour, ICoin
{
    // Use this for initialization
    void Start()
    {

    }

    void Update()
    {
        if (destroyedTime != 0)
        {
            // Coin was clicked:
            // it will shrink slowly
            var deltaTime = Time.time - destroyedTime;
            var speed = 5f;
            transform.localScale -= Vector3.one * (speed * deltaTime);
            if (deltaTime > 1)
            {
                Destroy(gameObject);
            }

        }
        else
        //if (dist < 100)
        {
            // rotate the coin
            var coin = transform.GetChild(0);
            coin.localPosition = new Vector3(0, 1 + Mathf.Sin(Time.time) * 0.2f, 0);
            coin.rotation = Quaternion.Euler(90, Time.time * 300, 0);
        }
    }

    float destroyedTime = 0;
    public void CollectCoin()
    {
        // only collect time, when it wasnt collected yet
        if (destroyedTime == 0)
        {
            MainGameInstance.Instance.CoinCount += 1;
            MainGameInstance.Instance.CollectedPoints.Add(transform.position.XZ());
            destroyedTime = Time.time;
        }
    }
}
