using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif
public class LevelGenerator : MonoBehaviour
{
    public string levelName
    {
        get
        {
            return LevelFile.name.Split('.')[0];
        }
    }
    public TextAsset LevelFile;
    public TextAsset GeometriesFile;
    public GameObject POIPrefab;
    public GameObject CoinPrefab;
    public GameObject MiniMapPipePrefab;
    public GameObject WayPoint;

    public bool ShouldPlaceCoins = false;
    // Start is called before the first frame update

    public Halak.JValue jval;
    void Start()
    {
        var root = Halak.JValue.Parse(LevelFile.text);

        foreach (var poi in root["points of interest"].ToArray())
        {
            var nw = GameObject.Instantiate(POIPrefab);

            if (POIPrefab != null)
            {
                var rw = nw.GetComponent<RealWorldLocation>();
                rw.lon = poi["longitude"];
                rw.lat = poi["latitude"];
                rw.updateLocationFromGPS();
                nw.name = "POI " + poi["POIName"].ToString();
            }


        }
        if (ShouldPlaceCoins)
            PlaceCoins();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public List<Vector2> GetLocations(string routeName)
    {

        var points = Halak.JValue.Parse(GeometriesFile.text).ToObject()[routeName]["coordinates"].ToArray();
        var locations = new List<Vector2>();
        foreach (var point in points)
        {
            float lon = (float)point[0].ToDouble();
            float lat = (float)point[1].ToDouble();

            var pos = MapAnchor.Instance.TransformFromWorldToUnity(new Vector2(lon, lat));
            locations.Add(pos);
        }

        return locations;
    }

    void PlaceCoins()
    {
        var locations = GetLocations(levelName);

        for (int i = 0; i < locations.Count - 2; i++)
        {
            var current = locations[i];
            var currentXY = new Vector3(current.x, 0, current.y);
            var next = locations[i + 1];
            var nextXY = new Vector3(next.x, 0, next.y);

            var distance = (current - next).magnitude;

            var piece = new GameObject();
            piece.transform.parent = this.transform;


            if (MiniMapPipePrefab != null)
            {
                var pipe = Instantiate(MiniMapPipePrefab, piece.transform);
                pipe.transform.position = new Vector3(current.x, 0, current.y);
                pipe.transform.localScale = new Vector3(distance, 1, 1);
                var angles = Quaternion.LookRotation(nextXY - currentXY).eulerAngles;
                var angle = angles.y -90;
                
                pipe.transform.rotation = Quaternion.Euler(0, angle, 0);

                

            }

        }
    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(LevelGenerator))]
public class LevelGeneratorEditorInspector : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        var gen = (LevelGenerator)target;
        if (GUILayout.Button("Print All Locations for MapBox"))
        {
            // https://docs.mapbox.com/playground/directions/
            string res = "";
            var root = Halak.JValue.Parse(gen.LevelFile.text);

            foreach (var poi in root["points of interest"].ToArray())
            {
                res += poi["longitude"] + "," + poi["latitude"] + ";";
            }
            Debug.Log("Coordinates:   " + res);
        }

        if (GUILayout.Button("Put POIs and WayPoints on Map"))
        {
           
            var points = new GameObject("Points of " + gen.LevelFile.name);
            var level = points.AddComponent<Level>();
            points.transform.parent = gen.transform;
            var root = Halak.JValue.Parse(gen.LevelFile.text);

           
            if (gen.POIPrefab != null)
            {
                int counter = 0;
                foreach (var poi in root["points of interest"].ToArray())
                {

                    var poiObject = GameObject.Instantiate(gen.POIPrefab);
                    poiObject.name = "(" + counter + ")" + poi["POIName"].ToString();
                    poiObject.transform.parent = points.transform;

                    var location = poiObject.GetComponent<RealWorldLocation>();
                    location.lon = (float)poi["longitude"];
                    location.lat = (float)poi["latitude"];
                    location.updateLocationFromGPS();

                    var poiComponent = poiObject.GetComponent<PoiInformation>();
                    poiComponent.LoadFromFile(counter, gen.LevelFile);

                    counter++;
                }
            }
            if (gen.WayPoint != null)
            {
                int counter = 0;
                foreach (var wp in gen.GetLocations(gen.levelName))
                {
                    var wayPointObject = GameObject.Instantiate(gen.WayPoint);
                    wayPointObject.name = "Waypoint(" + counter + ")";
                    wayPointObject.transform.parent = points.transform;

                    var location = wayPointObject.GetComponent<RealWorldLocation>();
                    wayPointObject.transform.position = new Vector3(wp.x, 0, wp.y);
                    location.updateGPSfromUnity();

                    counter++;
                }
            }

        }
    }

}
#endif