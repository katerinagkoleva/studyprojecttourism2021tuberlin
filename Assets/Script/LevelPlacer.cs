using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPlacer : MonoBehaviour
{
    public Level Level;
    public GameObject NormalCoinPrefab;
    public GameObject SpecialCoinPrefab;
    // Start is called before the first frame update
    void Start()
    {
        if (MainGameInstance.Instance.CurrentLevel != null)
        {
            Level = MainGameInstance.Instance.CurrentLevel;
        } else if (Level != null)
        {
            MainGameInstance.Instance.CurrentLevel = Level;
        }
        PlaceCoins(MainGameInstance.Instance.CurrentSelectedPoi);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void PlaceCoins(PoiInformation CurrentPoiGoal)
    {
        var locations = Level.GetLevelPartFromPoiOn(CurrentPoiGoal);


        float coinDistance = 10;
        for (int i = 0; i < locations.Count - 1; i++)
        {
            var current = locations[i];
            var currentXY = new Vector3(current.x, 0, current.y);
            var next = locations[i + 1];
            var nextXY = new Vector3(next.x, 0, next.y);

            var distance = (current - next).magnitude;

            var piece = new GameObject();
            piece.transform.parent = this.transform;
            if (NormalCoinPrefab != null)
            {
                for (float d = 0; d < distance; d += coinDistance)
                {
                    var newPosition = Vector2.Lerp(current, next, d / distance);
                    if (MainGameInstance.Instance.CollectedPoints.Contains(newPosition))
                    {
                        continue;
                        // When coin was already collected, don't recreate it!
                    }
                    var newCoin = Instantiate(NormalCoinPrefab, piece.transform);
                    newCoin.transform.position = new Vector3(newPosition.x, 0, newPosition.y);
                }
            }

        }

        var nextPoi = Level.GetNextPOI(CurrentPoiGoal);
        if (SpecialCoinPrefab != null && nextPoi != null)
        {
            var newCoin = Instantiate(SpecialCoinPrefab, this.transform);
            newCoin.transform.position = nextPoi.transform.position;
            newCoin.GetComponent<SpecialCoin>().POI = nextPoi;
        }
    }
}
