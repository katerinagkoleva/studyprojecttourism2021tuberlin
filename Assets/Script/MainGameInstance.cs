﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MainGameInstance : MonoBehaviour
{
    public float TimeStart = 0;
    //public Text Counter;
    public bool timerActive = false;
    public string UserName ="";
    public int CoinCount = 0;
    /// <summary>
    /// Current Route that is selected
    /// </summary>
    public string Route;
    public Level CurrentLevel;

    public PoiInformation CurrentSelectedPoi = null;

    public Player Player = null;

    /// <summary>
    /// Contains all Coins that were Collected
    /// </summary>
    public List<Vector2> CollectedPoints = new List<Vector2>();


    private static MainGameInstance _instance;
    public static MainGameInstance Instance
    {
        get
        {
            if (_instance == null)
            {
                var _new = new GameObject();
                _new.name = "MainGame";
                var comp =_new.AddComponent<MainGameInstance>();
                DontDestroyOnLoad(_new);
                _instance = comp;
            }
            return _instance;
        }
    }
    // Use this for initialization
    void Start()
    {
        Debug.Log("MainGame started");
        if (_instance != null && (_instance != this))
        {
            throw new System.Exception("MainGame alreday instanced");
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(timerActive == true){
        TimeStart += Time.deltaTime;
        }
    }
}
