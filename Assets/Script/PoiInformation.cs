﻿using UnityEngine;
using System.Collections;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class PoiInformation : MonoBehaviour
{
    public float Latitude;
    public float Longitude;


    public string Name;
    public string Question;
    public string[] Answers = new string[4];
    public int CorrectAnswer = 0;
    public string Description;
    public string ShortDescription;
    public int ID;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void LoadFromFile(int id, TextAsset route)
    {
        var root = Halak.JValue.Parse(route.text);

        foreach (var poi in root["points of interest"].ToArray())
        {
            if (poi["ID"] == id)
            {
                var answers = poi["Answers"].ToArray()[0];

                // Get Answers in JSON and set text in the right TextField
                var panel = transform.Find("Panel");
                for (int i = 0; i < 4; i++)
                {
                    var letters = "ABCD".ToCharArray();
                    var answerObj = answers.ToObject()
                        [letters[i].ToString()][0].ToObject();
                    string answer = answerObj
                        ["textAnswer"];

                    if (answerObj["isCorrect"].ToBoolean() == true)
                    {
                        CorrectAnswer = i;
                    }


                    Answers[i] = answer;
                }

                // Set Question Text

                Question = poi["question"];
                Longitude = (float)poi["longitude"];
                Latitude = (float)poi["latitude"];
                Description = poi["description"];
                Name = poi["POIName"];
                ID = poi["ID"];
                ShortDescription = poi["shortDescription"];

            }

        }
    }




}


#if UNITY_EDITOR

[CustomEditor(typeof(PoiInformation))]
public class PoiInformationInspector : Editor
{

    public TextAsset routeFile;
    public int ID;
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        PoiInformation poi = (PoiInformation)target;

        EditorGUILayout.Separator();
        EditorGUILayout.BeginHorizontal();
        routeFile = (TextAsset)EditorGUILayout.ObjectField(routeFile, typeof(TextAsset), true);
        ID = EditorGUILayout.IntField(ID);
        if (GUILayout.Button("Load"))
        {
            poi.LoadFromFile(ID, routeFile);
        }
        EditorGUILayout.EndHorizontal();
    }

}
#endif