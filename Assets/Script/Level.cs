using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public PoiInformation GetNextPOI(PoiInformation current)
    {
        int from = GetIndexOfPOI(current);
        for (int i = from + 1; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);
            var poi = child.GetComponent<PoiInformation>();
            if (poi != null)
                return poi;

        }
        return null;
    }

    int GetIndexOfPOI(PoiInformation poi)
    {

        if (poi != null)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                var child = transform.GetChild(i);
                var poi_i = child.GetComponent<PoiInformation>();
                if (poi_i != null && poi_i==poi)
                {
                    return i;
                }

            }
            throw new System.Exception("POI Not Found");
        } else
        {
            return 0;
        }
    }

    public List<Vector2> GetLevelPartFromPoiOn(PoiInformation poi, MapAnchor map = null)
    {
        var res = new List<Vector2>();

        int from = GetIndexOfPOI(poi);

        for (int i = from; i < transform.childCount; i++)
        {
            var child = transform.GetChild(i);

            var normalCoin = child.GetComponent<RealWorldLocation>();
            if (normalCoin != null)
            {
                Vector2 pos;
                if (map != null)
                {
                    pos = normalCoin.GetPositionForMap(map);
                } else
                {
                    pos = normalCoin.transform.position.XZ();
                }
                res.Add(pos);
            }
            var nextPoi = child.GetComponent<PoiInformation>();
            if (nextPoi != null && nextPoi != poi)
                break;

        }

        return res;

    }


   

}
