using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraWaypoint : MonoBehaviour
{

    public GameObject NewObject;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    float elapsed=0;
    // Update is called once per frame
    void Update()
    {
        if (transform.hasChanged && Time.frameCount%5 ==0)
        {
            
            var next = Instantiate(NewObject, this.transform.position, new Quaternion(), GameObject.Find("WayPoints").transform);
            
            transform.hasChanged = false;
        }

    }
}
