using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MiniMapRoute : MonoBehaviour
{
    public GameObject RoutePipePrefab;
    public GameObject PoiPrefab;
    public MapAnchor Anchor;
    public Level Level;

    // Start is called before the first frame update
    void Start()
    {

        
    }


    private Level currentLevel = null;
    void DrawRoute()
    {
        if (Level == null)
            return;
        if (currentLevel == Level)
        {
            // This level was already drawn
            return;
        }
        currentLevel = Level;
        var locations = Level.GetLevelPartFromPoiOn(MainGameInstance.Instance.CurrentSelectedPoi, Anchor) ;

        // iterate over all Waypoints and place "pipes"
        for (int i = 0; i < locations.Count - 1; i++)
        {
            var current = locations[i];
            var currentXY = new Vector3(current.x, 0, current.y);
            var next = locations[i + 1];
            var nextXY = new Vector3(next.x, 0, next.y);

            var distance = (current - next).magnitude;

            var piece = new GameObject("MiniMapLevelPart");
            piece.transform.parent = this.transform;
            piece.transform.localPosition = new Vector3();


            if (RoutePipePrefab != null)
            {
                var pipe = Instantiate(RoutePipePrefab, piece.transform);
                pipe.transform.position = new Vector3(current.x, piece.transform.position.y+0.01f, current.y);
                pipe.transform.localScale = new Vector3(distance, 0.01f, 0.01f);
                var angles = Quaternion.LookRotation(nextXY - currentXY).eulerAngles;
                var angle = angles.y - 90;

                pipe.transform.rotation = Quaternion.Euler(0, angle, 0);
            }

        }
    }

    // Update is called once per frame
    void Update()
    {
        Level = MainGameInstance.Instance.CurrentLevel;
        DrawRoute();
    }
}
