using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Raycast_Buildings : MonoBehaviour
{
    // Start is called before the first frame update

    public GameObject[] buildings;  // alle Gameobjects die den Tag "TU-Building" haben sollen hier gespeichert werden
    public GameObject Info_text; // Anzuzeigender Info-Text;
    public GameObject Text_bg;

    public TextAsset BuildingsFile;

    void Start()
    {
        Info_text = GameObject.FindGameObjectWithTag("Building_Info");
        Text_bg = GameObject.FindGameObjectWithTag("Text_Bg");

        Info_text.GetComponent<Text>().text = "Click Building to get Information!";

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount != 0)
        {
            var touch_var = Input.GetTouch(0);
            if (touch_var.phase == TouchPhase.Began)
            {
                Click(touch_var.position);
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            Click(Input.mousePosition);
        }
    }

    void Click(Vector2 position)
    {
        RaycastHit hit;
        Ray ray = Camera.main.ScreenPointToRay(position); // ray = Vector der von TouchPosition in die Scene geworfen wird
        
        if (Physics.Raycast(ray, out hit))
        {
            if (Info_text.activeSelf == false && hit.transform.CompareTag("TU-Building"))
            {
                Info_text.SetActive(true);
                Text_bg.SetActive(true);
            }
            string name = GetBuilding(hit.transform.name)["Name"];
            string description = GetBuilding(hit.transform.name)["Description"];

            Info_text.GetComponent<Text>().text = name + "\n" + description;
        }
        else
        {
            Info_text.SetActive(false);
            Text_bg.SetActive(false);
        }
    }

    Halak.JValue GetBuilding(string BlenderName)
    {
        var root = Halak.JValue.Parse(BuildingsFile.text);
        foreach (var obj in root["TU-Buildings"].ToArray())
        {
            if (obj["BlenderName"] == BlenderName)
            {
                return obj;
            }
        }
        return null;
    }
}
