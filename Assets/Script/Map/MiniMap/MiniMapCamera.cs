using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MiniMapCamera : MonoBehaviour
{
    public Slider Slider;
    public GameObject MainPlayer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<Camera>().orthographicSize = Slider.value;
        transform.position = new Vector3(MainPlayer.transform.position.x, transform.position.y, MainPlayer.transform.position.z);
    }
}
