using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MiniMapInteraction : MonoBehaviour, IPointerClickHandler
{
    bool full = false;
    Vector2 oldSize;
    public void OnPointerClick(PointerEventData eventData)
    {
        var rect = GetComponent<RectTransform>();
        if (full)
        {
            rect.sizeDelta = oldSize;
            full = false;
        } else
        {
            oldSize = rect.sizeDelta;
            var parentSize = transform.parent.GetComponent<RectTransform>().sizeDelta;
            var min = Mathf.Min(parentSize.x, parentSize.y);
            rect.sizeDelta = new Vector2(min,min);
            full = true;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
