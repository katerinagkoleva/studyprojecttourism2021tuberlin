using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ARMiniMapPlayer : MonoBehaviour
{
    public Player RealPlayer;
    // Start is called before the first frame update
    void Start()
    {
     
    }

    // Update is called once per frame
    void Update()
    {
       
        
        RealPlayer = MainGameInstance.Instance.Player;
        
        if (RealPlayer == null || RealPlayer.PlayerMapAnchor == null)
            return;
        var realLoc = RealPlayer.PlayerMapAnchor.TransformFromUnityToWorld(
            new Vector2(
                RealPlayer.transform.position.x,
                RealPlayer.transform.position.z
            ));
        var miniPlayer = GetComponent<RealWorldLocation>();
        miniPlayer.lat = realLoc.y;
        miniPlayer.lon = realLoc.x;
        miniPlayer.updateLocationFromGPS();

        transform.localRotation = Quaternion.Euler(0, RealPlayer.transform.rotation.eulerAngles.y, 0);
        
    }
}
