﻿using UnityEngine;
using System.Collections;
#if UNITY_ANDROID
using UnityEngine.Android;
#endif
public class LocationProvider : MonoBehaviour
{
    public bool isUnityRemote = false;
    // Use this for initialization
    void Start()
    {
#if UNITY_ANDROID

        if (!Permission.HasUserAuthorizedPermission(Permission.FineLocation))
        {
            Permission.RequestUserPermission(Permission.FineLocation);
        }
#endif
        StartCoroutine(StartLocationService(1, 1));
    }

    private IEnumerator StartLocationService(float desiredAccuracyInMeters, float updateDistanceInMeters)
    {
        // Wait until the editor and unity remote are connected before starting a location service
        if (isUnityRemote)
        {
            yield return new WaitForSeconds(5);
        }

        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            Debug.Log("No locations enabled in the device");
            yield break;
        }

        // Start service before querying location
        Input.location.Start();

        if (isUnityRemote)
        {
            yield return new WaitForSeconds(5);
        }

        // Wait until service initializes
        int maxWait = 20;
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            Debug.Log("Service didn't initialize in 20 seconds");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.Log("Unable to determine device location");
            yield break;
        }
        // Access granted and location value could be retrieved
        else
        {
            float lat = Input.location.lastData.latitude;
            float lon = Input.location.lastData.longitude;
            Input.compass.enabled = true;
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
