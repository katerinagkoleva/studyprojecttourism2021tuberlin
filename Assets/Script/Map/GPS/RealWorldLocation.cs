using System.Collections;
using System.Collections.Generic;

using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif


[ExecuteInEditMode]
public class RealWorldLocation : MonoBehaviour
{
    /// <summary>
    /// Y Component 
    /// </summary>
    public float lat;
    /// <summary>
    /// X Component
    /// </summary>
    public float lon;

    public MapAnchor MapAnchor = null;

    public MapAnchor MapAnchorOrDefault
    {
        get
        {
            if (MapAnchor != null)
                return MapAnchor;
            return MapAnchor.Instance;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (transform.hasChanged)
        {
            updateGPSfromUnity();
            transform.hasChanged = false;
        }
    }

    private void OnValidate()
    {
#if UNITY_EDITOR
        updateLocationFromGPS();
#endif
    }

    public void updateLocationFromGPS()
    {
        var anchor = MapAnchorOrDefault;

        if (anchor != null)
        {
            var loc = anchor.TransformFromWorldToUnity(new Vector2(lon, lat));
            this.transform.position = new Vector3(loc.x, transform.position.y, loc.y);
        }

    }

    public void updateGPSfromUnity()
    {
        var anchor = MapAnchorOrDefault;
        if (anchor != null)
        {
            var world = anchor.TransformFromUnityToWorld(new Vector2(transform.position.x, transform.position.z));
            lon = world.x;
            lat = world.y;
        }

    }

    public Vector2 GetPositionForMap(MapAnchor map)
    {
        return map.TransformFromWorldToUnity(new Vector2(lon, lat));
    }
}

#if UNITY_EDITOR

[CustomEditor(typeof(RealWorldLocation))]
public class RealWorldLocationButton : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        RealWorldLocation myScript = (RealWorldLocation)target;
        if (GUILayout.Button("Update Location from GPS"))
        {
            myScript.updateGPSfromUnity();
        }
    }

}
#endif