using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class MapAnchor : MonoBehaviour
{
    private static MapAnchor _Instance;
    public static MapAnchor Instance
    {
        get
        {
            if (_Instance == null)
            {
                _Instance = GameObject.FindGameObjectWithTag("Origin")?.GetComponent<MapAnchor>();
            }
            return _Instance;
        }
    }

    public LocationAnchor Location1;
    public LocationAnchor Location2;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    //https://gis.stackexchange.com/questions/298619/mercator-map-coordinates-transformation-formula
    static Vector2 project(Vector2 p)
    {
        float a = 6378137; // (Equatorial radius)

        float Mercator_x = a * p.x;


        float Mercator_y = a * Mathf.Log(Mathf.Tan(Mathf.PI / 4 + Mathf.Deg2Rad * p.y / 2));
        return new Vector2(Mercator_x, Mercator_y);

    }

    static Vector2 projectInv(Vector2 p)
    {

        float a = 6378137; // (Equatorial radius)


        float x = p.x / a;
        float y = 2 * Mathf.Atan(Mathf.Pow(Mathf.Exp(1), p.y / a)) - (Mathf.PI / 2);
        return new Vector2(x, Mathf.Rad2Deg * y);
    }

    public struct ViewPort
    {
        public float scaleX;
        public float scaleY;
        public float offsetX;
        public float offsetY;

        public ViewPort(float scaleX, float scaleY, float offsetX, float offsetY)
        {
            this.scaleX = scaleX;
            this.scaleY = scaleY;
            this.offsetX = offsetX;
            this.offsetY = offsetY;
        }
    }

    static ViewPort viewport(Vector2 p1_from, Vector2 p1_to, Vector2 p2_from, Vector2 p2_to)
    {

        p1_from = project(p1_from);
        p2_from = project(p2_from);
        float diffX_1 = p1_from.x - p2_from.x;
        float diffX_2 = p1_to.x - p2_to.x;
        float diffY_1 = p1_from.y - p2_from.y;
        float diffY_2 = p1_to.y - p2_to.y;
        float scaleX = diffX_2 / diffX_1;
        float scaleY = diffY_2 / diffY_1;
        float offsetX = +p2_to.x - p2_from.x * scaleX;


        float offsetY = +p2_to.y - p2_from.y * scaleY;
        
        return new ViewPort(scaleX, scaleY, offsetX, offsetY);
    }

    public Vector2 TransformFromWorldToUnity(Vector2 p)
    {
        var trafo = CurrentViewPort;
        p = project(p);
        p = new Vector2(p.x * trafo.scaleX + trafo.offsetX, p.y * trafo.scaleY + trafo.offsetY);
        return p;
    }

    public Vector2 TransformFromUnityToWorld(Vector2 p)
    {
        var trafo = CurrentViewPort;
        p = new Vector2((p.x - trafo.offsetX) / trafo.scaleX, (p.y - trafo.offsetY) / trafo.scaleY);
        p = projectInv(p);
        return p;

    }

    //https://stackoverflow.com/questions/18883601/function-to-calculate-distance-between-two-coordinates
    //This function takes in latitude and longitude of two location and returns the distance between them as the crow flies (in km)
    public static float CalcDistanceBetweenCoords(float lat1, float lon1, float lat2, float lon2)
    {
        var R = 6378137; //  (Equatorial radius) in m
        var dLat = toRad(lat2 - lat1);
        var dLon = toRad(lon2 - lon1);
        lat1 = toRad(lat1);
        lat2 = toRad(lat2);

        var a = Mathf.Sin(dLat / 2) * Mathf.Sin(dLat / 2) +
          Mathf.Sin(dLon / 2) * Mathf.Sin(dLon / 2) * Mathf.Cos(lat1) * Mathf.Cos(lat2);
        var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));
        var d = R * c;
        return d;
    }

    // Converts numeric degrees to radians
    static float toRad(float Value)
    {
        return Value * Mathf.PI / 180;
    }

    public ViewPort CurrentViewPort
    {
        get
        {
            return
                viewport(
                    new Vector2(Location1.lon, Location1.lat),
                        new Vector2(
                            Location1.transform.position.x,
                            Location1.transform.position.z
                        ),
                    new Vector2(Location2.lon, Location2.lat),
                        new Vector2(
                            Location2.transform.position.x,
                            Location2.transform.position.z
                        ));
        }
    }

    //ViewPort trafo = v

   // Vector2 worldPos = transformToWorld(trafo, project(new Vector2(Input.location.lastData.latitude, Input.location.lastData.longitude)));
}
#if UNITY_EDITOR

[CustomEditor(typeof(MapAnchor))]
public class MapAnchorButton : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        MapAnchor map = (MapAnchor)target;
        if (GUILayout.Button("Scale To RealSize"))
        {
            
            float realDistanceX = MapAnchor.CalcDistanceBetweenCoords(
                map.Location1.lat,
                map.Location1.lon,
                map.Location1.lat,
                map.Location2.lon
                );

            float realDistanceY = MapAnchor.CalcDistanceBetweenCoords(
               map.Location1.lat,
               map.Location1.lon,
               map.Location2.lat,
               map.Location1.lon
               );

            float unityDistanceX = (map.Location2.transform.position.x - map.Location1.transform.position.x);
            float unityDistanceY = (map.Location2.transform.position.z - map.Location1.transform.position.z);
            float scaleX = realDistanceX / unityDistanceX;
            float scaleY = realDistanceY / unityDistanceY;
            Debug.Log("x " + scaleX + " y" + scaleY+"; "+realDistanceY+"; " + unityDistanceY);

            map.transform.localScale = new Vector3(map.transform.localScale.x*scaleX, map.transform.localScale.y, map.transform.localScale.z * scaleY);
        }
    }

}
#endif