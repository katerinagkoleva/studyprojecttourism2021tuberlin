using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Anchor Position to define Map Scaling and Transformation from Untity Location to real Location
/// </summary>
public class LocationAnchor : MonoBehaviour
{
    public float lat;
    public float lon;
}