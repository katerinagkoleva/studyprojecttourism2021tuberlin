
# Study Project

## Build and run

Download all the files from this repository. Either via the zip download or with `git clone` 
Open the project with Unity 2021.1.5f1 by using Unity Hub and adding the project folder.


You now can build the app by clicking File -> Build Settings. Select the Target Platform and click Build.


Link to [DEMO](https://www.youtube.com/watch?v=PBKt00LPQxM)

This project is made by Alexandra Sipos, Christoph Penno, Eduardo Luiz Rhein, Jonnatan Wendler, Katerina Georgieva Koleva, Sandra Phuong Nhi Vu, Nicole Stefanie Bertges

A paper, based on this project is published in the HCI International Conference 2022 - [Towards Improvement of UX Using Gamification for Public Artistic and Historical Artifacts in AR](https://dl.acm.org/doi/abs/10.1007/978-3-031-06015-1_10)

